﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HumanSaveCounter : MonoBehaviour {

    private int _saveConuter = 0;

    public int GetSaveCounter { get { return _saveConuter; } }

    private Text _countText;


	void Start () {
        _countText = GetComponent<Text>();
        _countText.text = _saveConuter.ToString();
	}
	
	private void OnHumanSave()
    {
        _saveConuter++;
        _countText.text = _saveConuter.ToString(); 
    }

    private void OnEnable()
    {
        SurvivorFoLLow.Rescued += OnHumanSave;
    }

    private void OnDisable()
    {
        SurvivorFoLLow.Rescued -= OnHumanSave;
    }
}
