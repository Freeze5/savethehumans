﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour {

    public GameObject bulletPref;
    public Transform bulletSpawner;
    [SerializeField]
    private Animator _animator;
    [SerializeField]
    private float _bulletSpeed;
    private float _timeToDestroy = 1.5f;

    [SerializeField]
    private int _currentAmmo ;
    private const int _maxAmmo = 20;
    private float _reloadTime = 1f;
    private bool isReloading = false;

    public int GetCurrentAmmo { get { return _currentAmmo; } }

    
    private AudioSource _audioSource;
    [SerializeField]
    private AudioClip _shootingClip, _reloadClip;

    private Rigidbody _rigidBody;
    [SerializeField]
    private ParticleSystem _fireParticles, _smokeParticles;
    [SerializeField]
    private PlayerController _player;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _currentAmmo = _maxAmmo;
    }

    public void ShootOn()
    {
        if(_player.GetIsDead == false)
        {
            if (_currentAmmo > 0 && !isReloading)
            {
                GameObject bullet = Instantiate(bulletPref, bulletSpawner.position, transform.rotation) as GameObject;

                Rigidbody rb = bullet.GetComponent<Rigidbody>();

                rb.AddForce(transform.forward * _bulletSpeed);

                _animator.SetBool("isShoot", true);
                _currentAmmo--;

                _audioSource.PlayOneShot(_shootingClip);
                Destroy(bullet, _timeToDestroy);
                _fireParticles.Play();
                _smokeParticles.Play();
            }
            else
            {
                //reload method
                StartCoroutine(Reload());
            }
        }
        else
        {
            return;
        }
    }

    public void ShootOff()
    {
        _animator.SetBool("isShoot", false);
    }

    IEnumerator Reload()
    {
        Debug.Log("Reloading.......");
        isReloading = true;
        _audioSource.PlayOneShot(_reloadClip);
        yield return new WaitForSeconds(_reloadTime);
        _currentAmmo = _maxAmmo;
        isReloading = false;
    }
	
	
}
