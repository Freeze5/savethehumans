﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstAid : MonoBehaviour {

    private readonly string PlayerTag = "Player";
    [SerializeField]
    private float _aidValue = 35f;

    public static System.Action<FirstAid> Consume;

    private void Awake()
    {
        gameObject.SetActive(false);
    }

    public void Spawn( Vector3 position)
    {
        transform.position = position;
        gameObject.SetActive(true);
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == PlayerTag)
        {
            col.gameObject.GetComponent<PlayerController>().Heal(_aidValue);
            gameObject.SetActive(false);
            
            if (Consume != null)
            {
                Consume(this);
            }
        }
    }
}
