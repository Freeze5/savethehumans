﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstAidPoolManager : MonoBehaviour {

    private static FirstAidPoolManager _instance;

    [SerializeField]
    private GameObject _firstAidPrefab;


    private List<FirstAid> _avaliable;

    private List<FirstAid> _busy;

    private const int MIN_POOL_SIZE = 3;

    public static FirstAidPoolManager Instance { get { return _instance; } }

    private void Awake()
    {
        _instance = this;
        _avaliable = new List<FirstAid>();
        _busy = new List<FirstAid>();

        for ( int i =0; i < MIN_POOL_SIZE; i++)
        {
            AddAidToPool();
        }
    }

    void AddAidToPool()
    {
        _avaliable.Add(Instantiate(_firstAidPrefab).GetComponent<FirstAid>());
    }

    private void OnEnable()
    {
        FirstAid.Consume += OnConsume;
    }
    private void OnDisable()
    {
        FirstAid.Consume -= OnConsume;
    }

    private void OnConsume ( FirstAid obj)
    {
        _busy.Remove(obj);
        _avaliable.Add(obj);

    }

    public void SpawnFirstAid ( Vector3 position)
    {
        if(_avaliable.Count == 0)
        {
            AddAidToPool();
        }

        FirstAid item = _avaliable[_avaliable.Count - 1];
        item.Spawn(position);
        _busy.Add(item);
        _avaliable.Remove(item);
    }
}
