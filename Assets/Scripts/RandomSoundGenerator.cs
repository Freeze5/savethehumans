﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class RandomSoundGenerator : MonoBehaviour {

    [SerializeField]
    private List<AudioClip> _audioClips;
    private AudioSource _audio;
    private AudioClip _randomClip {
        get
        {
            return _audioClips[(Random.Range(0,_audioClips.Count))];
        }
    }

    [SerializeField]
    private Enemy _enemy;

    private float _minTimeBetweenPlayingClip = 15f;
    private float _maxTimeBetweenPlayingClip = 25f;

    void Start () {
        _audio = GetComponent<AudioSource>();
        StartCoroutine(PlayRandomSound());
	}

    void StopPlaying()
    {
        _audio.Stop();
    }

    void StartPlying( AudioClip clip)
    {
        _audio.clip = clip;
        _audio.Play();
    }

    IEnumerator PlayRandomSound()
    {
        if (!_enemy.GetIsDead)
        {
            if (!_audio.isPlaying)
            {
                StartPlying(_randomClip);
            }
            yield return new WaitForSeconds(Random.Range(_minTimeBetweenPlayingClip,_maxTimeBetweenPlayingClip));
            StartCoroutine(PlayRandomSound());
        }
        else
        {
            StopPlaying();
        }
    }
	
	
}
