﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField]
    private Transform _playerTransform;
    private Vector3 _cameraOffset;
    [Range(0.01f, 1.0f)]
    public float smoothFactor = 0.5f;
    public bool lookAtPlayer = true;

    private void Start()
    {
        _cameraOffset = transform.position - _playerTransform.position;//расстояние между камерой и игроком
    }

    private void LateUpdate()
    {
        Vector3 newPos = _playerTransform.position + _cameraOffset;//задаем новую позицию камеры во время следования за игроком с учетом оффсета
        transform.position = Vector3.Slerp(transform.position, newPos, smoothFactor);// метод Slerp обезпечивает плавное движение
        if (lookAtPlayer)
            transform.LookAt(_playerTransform);
    }


}
