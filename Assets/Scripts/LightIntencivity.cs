﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightIntencivity : MonoBehaviour {

    private Light _light;

    [SerializeField]
    private float _minIntensity = 0.5f, _maxIntensity = 2f;
    



	void Start () {
        _light = GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
        _light.intensity = Mathf.Lerp(_minIntensity, _maxIntensity, Mathf.PingPong(Time.time * 2, 1));
	}
}
