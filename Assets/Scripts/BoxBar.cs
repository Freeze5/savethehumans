﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxBar : BaseHealthBar{
    
    private float _xValue;


    private void Start()
    {
        _xValue = transform.localScale.x;
    }

    public override void SetValue(float value, float maxValue)
    {
        base.SetValue(value, maxValue);
         
        transform.localScale = new Vector3(_xValue * value / maxValue,
            transform.localScale.y, transform.localScale.z);
    }
}
