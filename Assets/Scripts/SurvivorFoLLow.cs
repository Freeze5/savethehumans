﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurvivorFoLLow : MonoBehaviour {

    [SerializeField]
    private Transform _player;

    private float _distanceToSitDown = 10;
    private float _distanceToStandUp = 8;
    private float _distanceToGo = 4;
    private float _distancetoStop = 2.5f;

    private float _timeToDestroy = 3f;

    public static System.Action Rescued;

    private Animator _animator;

    [SerializeField]
    private float _moveSpeed =0.6f;
    private float _staySpeed = 0;

    private readonly string RESCUE_ZONE = "RescueZone";

	void Start () {
        _animator = GetComponent<Animator>();
        _player = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	
	void Update () {
        transform.LookAt(_player);
        float distaceToPlayer = Vector3.Distance(transform.position, _player.position);
        //Debug.Log(distaceToPlayer);
        
        if(distaceToPlayer <= _distanceToStandUp )
        {
            _animator.SetBool("isStandUp", true);
            _animator.SetBool("isSitDown", false);
        }
       
        if (distaceToPlayer <= _distanceToGo && distaceToPlayer >=_distancetoStop)
        {
            SurvivorMove(_moveSpeed);
            _animator.SetBool("isWalk", true);
            _animator.SetBool("isStandUp", false);
        }

        if(distaceToPlayer >= _distanceToSitDown)
        {
            SurvivorMove(_staySpeed);
            _animator.SetBool("isWalk", false);
            _animator.SetBool("isSitDown", true);
        }

        if(distaceToPlayer <= _distancetoStop)
        {
            SurvivorMove(_staySpeed);
            _animator.SetBool("isIdle", true);
            _animator.SetBool("isWalk",false);
        }
        else if( distaceToPlayer >= _distancetoStop && distaceToPlayer <=_distanceToGo)
        {
            SurvivorMove(_moveSpeed);
            _animator.SetBool("isIdle", false);
            _animator.SetBool("isWalk", true);
        }
	}

    private void SurvivorMove( float speed)
    {
        transform.position += (_player.position - transform.position).normalized * speed * Time.deltaTime;
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == RESCUE_ZONE)
        {
            
            StartCoroutine(ByeBye());
        }
    }

    IEnumerator ByeBye()
    {
        SurvivorMove(_staySpeed);
        _animator.SetTrigger("isSave");
        yield return new WaitForSeconds(_timeToDestroy);
        if (Rescued != null)
            Rescued();
        Destroy(this.gameObject);
    }
}
