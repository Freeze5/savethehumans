﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerInterface : MonoBehaviour {

    [SerializeField]
    private Text _ammoText;
    [SerializeField]
    private Shooting currentAmmo;
	
	
	// Update is called once per frame
	void Update () {
        _ammoText.text = currentAmmo.GetCurrentAmmo.ToString();
	}
}
