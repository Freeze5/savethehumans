﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KillCounter : MonoBehaviour {

    private int _killCounter = 0;
    
    private Text _counText;

    private void Awake()
    {
        _counText = GetComponent<Text>();
        _counText.text = _killCounter.ToString();

    }

    private void OnEnemyDeath()
    {
        _killCounter++;
        _counText.text = _killCounter.ToString();
    }

    private void OnEnable()
    {
        Enemy.OnDeath += OnEnemyDeath;
    }

    private void OnDisable()
    {
        Enemy.OnDeath -= OnEnemyDeath;
    }
}
