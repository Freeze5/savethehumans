﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class Enemy : MonoBehaviour {

    private NavMeshAgent _agent;

    private Animator _animator;
    private Collider _collider;
    [SerializeField]
    private bool isAttack = false;
    
    [SerializeField]
    private PlayerController _player;
    

    [SerializeField]
    private float _distanceToPlayer = 8f;
    [SerializeField]
    private float _distanceToAttack = 1.65f;

    private const float EPSILON = 0.01f;

    private bool IsNavMeshMoving { get { return _agent.velocity.magnitude > EPSILON; } }
    [SerializeField]
    private float _health;
    [SerializeField]
    private float _maxHealth = 250;

    private float _angrySpeed = 2.5f;
    private float _patrolSpeed = 2.15f;

    private BoxBar _healthBar;
    //вычисление тегов
    private readonly string Bullet = "Bullet";
    private readonly string Player = "Player";

    private bool isDead = false;
    public bool GetIsDead { get { return isDead; } }
    private float _timeBetweenAttack = 1f;

    private DamageScript _zombieDamage;
    [SerializeField]
    private ParticleSystem _bloodFX;
    private AudioSource _audio;
    [SerializeField]
    private AudioClip _bloodClip;
    private float _randomNumber =1;
    private float _deathTimeDelay =2f;

    public static System.Action OnDeath;

    [SerializeField]
    private Transform _respawnTransform;
    
    private float _activeMoveRadius = 15f;

    private const float RESPAWN_TIME = 10f;

    private Tween _respawnTween;
    private Vector3 _lastMovePosition;

    private bool isAngry = false;
    private const float MOVE_EPSILON = 2f;

    private float _lootProbability = 0.9f;

    

    void Start () {
        _agent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
        _health = _maxHealth;
        _healthBar = GetComponentInChildren<BoxBar>();
        _zombieDamage = GetComponent<DamageScript>();
        _audio = GetComponent<AudioSource>();
        _collider = GetComponent<Collider>();

        isAngry = false;

        _respawnTransform = transform.parent;
        _lastMovePosition = _respawnTransform.position;
        
	}
	
	void Update ()
    {
        
        if (!isDead)
        {
            float distanceToPlayer = Vector3.Distance(_player.transform.position, this.transform.position);
            float respawnDistance = Vector3.Distance(_respawnTransform.position, this.transform.position);
            
            if (distanceToPlayer < _distanceToPlayer && respawnDistance < _activeMoveRadius)
            {
                isAngry = true;
                Vector3 playerPos = _player.transform.position;
                _agent.speed = _angrySpeed;
                _agent.SetDestination(playerPos);
            }
            else
            {
                if (isAngry)
                {
                    _agent.SetDestination(_lastMovePosition);
                }
                isAngry = false;
                _agent.speed  = _patrolSpeed;
                MoveRandomly();
            }

            if (distanceToPlayer < _distanceToAttack && _player.GetIsDead == false)
            {

                _animator.SetBool("isAttack", true);
                
            }
            else
            {

                _animator.SetBool("isAttack", false);
                
            }
        }
        else
        {
            return;
        }
	}

    private void MoveRandomly()
    {
        if(Vector3.Distance(_lastMovePosition, this.transform.position) < MOVE_EPSILON)
        {
            _lastMovePosition = GetRandomPassivePoint();
            _agent.SetDestination(_lastMovePosition);
        }
    }

    private Vector3 GetRandomPassivePoint()
    {
        return new Vector3(
            _respawnTransform.position.x + Random.Range(0, _activeMoveRadius),
            _respawnTransform.position.y,
            _respawnTransform.position.z + Random.Range(0, _activeMoveRadius)
            );
    }
    //подписка на событие смерти игрока
    private void OnEnable()
    {
        PlayerController.OnDeath += OnPlayerDeath;
    }
    //отписка
    private void OnDisable()
    {
        PlayerController.OnDeath -= OnPlayerDeath;
    }
    void OnPlayerDeath()
    {
        isAttack = false;
    }

    private void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == Bullet)
        {
            GetDamage(col.gameObject.GetComponent<DamageScript>().GetDamage);
            _bloodFX.Play();
            if (_randomNumber == Random.Range(0, 2))
            {
                _audio.PlayOneShot(_bloodClip);
            }
        }
        if(col.gameObject.tag ==Player && _player.GetIsDead==false && !isDead)
        {
            
            isAttack = true;
            StartCoroutine(Attack());
        }
    }

    private void OnTriggerExit(Collider col)
    {
        if(col.gameObject.tag == Player)
        {
            isAttack = false;
        }
    }

    IEnumerator Attack()
    {
        if (isAttack)
        {
            _player.GetDamage(_zombieDamage.GetDamage);
        }
        yield return new WaitForSeconds(_timeBetweenAttack);
        StartCoroutine(Attack());
    }
    private void GetDamage (float damage)
    {
        _health = Mathf.Clamp(_health - damage, 0, _health);
        _healthBar.SetValue(_health, _maxHealth);
        if(_health <= 0)
        {
            EnemyDeath();
        }
    }

    private void EnemyDeath()
    {
        isDead = true;
        _collider.enabled = false;
        _animator.SetBool("isDead", true);
        // Destroy(this.gameObject, 2);
        StartCoroutine(ExecuteAfterTime(_deathTimeDelay));
        if (OnDeath != null)
            OnDeath();

        RespawnDelay();
        DropLoot();

    }

    IEnumerator ExecuteAfterTime( float time)
    {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(false);
    }

    private void DropLoot()
    {
        if(Random.value < _lootProbability)
        {
            FirstAidPoolManager.Instance.SpawnFirstAid( new Vector3(transform.position.x,transform.position.y +2f,transform.position.z));
        }
    }
    private void RespawnDelay()
    {
        if(_respawnTween != null)
        {
            _respawnTween.Kill();
        }
        _respawnTween = DOVirtual.DelayedCall(RESPAWN_TIME, () =>
        {
            if (Vector3.Distance(_player.transform.position, _respawnTransform.position) > _activeMoveRadius)
            {
                Respawn();
            }
            else
            {
                RespawnDelay();
            }
        });
    }

    private void Respawn()
    {
        transform.position = _respawnTransform.position;

       
        gameObject.SetActive(true);
        _collider.enabled = true;
        isDead = false;
        _health = _maxHealth;
        _healthBar.SetValue(_health, _maxHealth);
    }
}
