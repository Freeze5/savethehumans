﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BloodyScreenManager : MonoBehaviour {

    private static BloodyScreenManager _instance;
    public static BloodyScreenManager Instance { get { return _instance; } }

    public Image bloodyImg;
    
    private float _transition;
   
    private float _duration;
   
    private bool isShowing;
   
    private bool isInTransition;

    [SerializeField]
    private PlayerController _player;

    private float _checkPlayerHp;

    private void Awake()
    {
        _instance = this;
        isInTransition = true;
        bloodyImg.color = new Color(bloodyImg.color.a, bloodyImg.color.b, bloodyImg.color.g, 0);
    }
    
    public  void Fade ( bool isShowing, float duration)
    {
        this.isShowing = isShowing;
        this._duration = duration;
        isInTransition = true;
        _transition = (isShowing == true) ? 0 : 1;
    }
	
	void Update () {
        _checkPlayerHp = _player.GetCurrentHp;
        if (_checkPlayerHp > _player.GetHp)
        {
            Fade(false, 3f);
        }
        else if (_checkPlayerHp <= _player.GetHp)
        {
            Fade(true, 1.25f);
        }

        if (!isInTransition)
            return;
       
        _transition += (isShowing == true) ? Time.deltaTime * (1 / _duration) : -Time.deltaTime * (1 / _duration);
        bloodyImg.color = Color.Lerp(
           new Color(bloodyImg.color.a, bloodyImg.color.b, bloodyImg.color.g, 0),
           new Color(bloodyImg.color.a, bloodyImg.color.b, bloodyImg.color.g, 1), 
           _transition);

        if (_transition > 1 || _transition < 0)
            isInTransition = false;
	}

    
    
}
