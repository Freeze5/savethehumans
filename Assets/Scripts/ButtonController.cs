﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour {

    private Animator _buttonAnimator;
    private int _counter;
    private AudioSource _audioSource;

    [SerializeField]
    private AudioClip _slideDown, _slideUp;

    [SerializeField]
    private GameObject _menuPanel;

    private void Start()
    {
        _buttonAnimator = _menuPanel.GetComponent<Animator>();
        _audioSource = _menuPanel.GetComponent<AudioSource>();
        _counter = 1;
    }

    public void ButtonAnimation()
    {
        for(int i =0; i < _counter; i++)
        {
            if(i % 2==0)
            {
                _buttonAnimator.SetBool("isDown", true);
                _audioSource.PlayOneShot(_slideDown);
            }
            else
            {
                _buttonAnimator.SetBool("isDown", false);
                _audioSource.PlayOneShot(_slideUp);
            }
        }
        _counter++;
    }

    public void ButtonGameOver()
    {
        _buttonAnimator.SetBool("isDown", true);
        _audioSource.PlayOneShot(_slideDown);
    }

    public void ExitGame()
    {
        Application.Quit();   
    }
    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }
}
