﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;
using System.Linq;

public class PlayerController : MonoBehaviour {
    [SerializeField]
    private Animator _animator;
    private CharacterController _characterController;

    private float _gravity = 20f;
    private Vector3 _moveDirection = Vector3.zero;

    public float speed = 5.0f;

    public float rotationSpeed = 240f;

    [SerializeField]
    private ImageHealthBar _healthBar;
    private Collider _collider;

    public static System.Action OnDeath;//событие смерти
    [SerializeField]
    private float _health;
    private float _currentHp;

    public float GetHp { get { return _health; } }
    public float GetCurrentHp { get { return _currentHp; } }

    [SerializeField]
    private float _maxHealth = 150f;
    private bool isDead = false;
    public bool GetIsDead { get { return isDead; } }

    [SerializeField]
    private BoxCollider _bulletCollider;

    private AudioSource _audioSource;
    [SerializeField]
    private AudioClip _deathAudio , _medKit, _rescuing ,_gameOver;
    [SerializeField]
    private List<AudioClip> _hitAudioClips;
    public AudioClip _randomHitClip { get
        {
            return _hitAudioClips[Random.Range(0, _hitAudioClips.Count)];
        }
    }

    private float _soundProbability = 0.3f;

    [SerializeField]
    private HumanSaveCounter _humSaveCount;
    [SerializeField]
    private ButtonController _buttonController;
    
    

    private void Awake()
    {
        _characterController = GetComponent<CharacterController>();
        _collider = GetComponent<Collider>();
        _health = _maxHealth;
        _currentHp = _health;
        _audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (!isDead)
        {
            Move();
        }
        else
        {
            return;
        }
        CheckRescued();
        
    }

    void Move () {
        float vertical = CrossPlatformInputManager.GetAxis("Vertical");
        float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");

        //узнаем куда направлена камера в плоскости x,z и определяем направление движеие персонажа в ту сторону , куда смотрит джойстик
        Vector3 camForward_Direction = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;
        Vector3 move = vertical * camForward_Direction + horizontal * Camera.main.transform.right;// узнаем как сильно и куда наклонен джойстик

        //ограничиваем вектор наклона джойстика
        if (move.magnitude > 1)
            move.Normalize();

        move = transform.InverseTransformDirection(move);//приводим координаты наклона джойстика с глобальных в локальные

        //узнаем угол на который должен повернутся персонаж вслед за джойстиком
        float turnAmount = Mathf.Atan2(move.x, move.z);

        //поворачиваем персонажа на заданный угол со скоростью назначенной в пременных
        transform.Rotate(0, turnAmount * rotationSpeed * Time.deltaTime, 0);

        if (_characterController.isGrounded)
        {
            _moveDirection = transform.forward * move.magnitude;
            _moveDirection *= speed;
        }
       
        //имитируем гравитацию перса в игровом пространстве
        _moveDirection.y -= _gravity * Time.deltaTime;

        _characterController.Move(_moveDirection * Time.deltaTime);

        // узнаем скорость персонажа
        Vector3 horizontalVelocity = _characterController.velocity;
        horizontalVelocity = new Vector3(_characterController.velocity.x, 0, _characterController.velocity.z);

        float horizontalSpeed = horizontalVelocity.magnitude;
        _animator.SetFloat("Speed", horizontalSpeed);

        
	}
    
    public void GetDamage(float damage)
    {
        //отнимаем от текущего здоровья кол-во едениц атаки врага и ограничиваем значение здоровья до 0
        _health = Mathf.Clamp(_health - damage, 0f, _health);
        _healthBar.SetValue(_health, _maxHealth);
        StartCoroutine(timeWaiter());
        if (Random.value < _soundProbability)
        {
            _audioSource.PlayOneShot(_randomHitClip);
        }
        if (_health <= 0)
        {
            PlayerDeath();
            StartCoroutine(GameOverSound());
        }
    }

    IEnumerator timeWaiter()
    {
        yield return new WaitForSeconds(3f);
        _currentHp = _health;
    }

    IEnumerator GameOverSound()
    { 
        yield return new WaitForSeconds(2f);
        _audioSource.PlayOneShot(_gameOver);
        _buttonController.ButtonGameOver();
    }

    public void  Heal ( float heal)
    {

        _health = Mathf.Clamp(_health + heal, 0, _maxHealth);
        _healthBar.SetValue(_health, _maxHealth);
        StartCoroutine(timeWaiter());
        _audioSource.PlayOneShot(_medKit);
    }
    
    private void PlayerDeath()
    {
        isDead = true;
        _collider.enabled = false;
        _audioSource.PlayOneShot(_deathAudio);
        if(OnDeath != null)
        {
            OnDeath();
        }
    }

    //переменные для отслеживание спасенных
    int _rescueCount = 0;
    int _updatedRescueCount;
    void CheckRescued()
    {
        _updatedRescueCount = _humSaveCount.GetSaveCounter;
        if(_updatedRescueCount > _rescueCount)
        {
            _audioSource.PlayOneShot(_rescuing);
            _rescueCount = _updatedRescueCount;
        }

    }
}
