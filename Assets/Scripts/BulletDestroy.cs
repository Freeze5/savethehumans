﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDestroy : MonoBehaviour {
   

    private readonly string _enemyTag = "Enemy";

    
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == _enemyTag)
            
            Destroy(this.gameObject);
    }
}
